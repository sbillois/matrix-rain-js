
const myLeters = '私は、「トークショー」、取り調べと自白の形式、すすり泣く司会者（拍手）に完全に恐怖を感じています。司会者の同情は、トランスジェンダーとしての私の人生に内在する悲劇を強調しています。 そしてこの瞬間は、人種やセクシュアリティのスペクトルを見ることを拒否したのとまったく同じ盲目的な方法で、ジェンダーのスペクトルを認めることを拒否する社会の病理を決して問うことなく、受け入れへの拒絶のカタルシスの弧を遂げています。';

const matrixRain = (letters, colors) => {

    // Initialisation du canevas
    let canvas = document.querySelector('canvas'),
        ctx = canvas.getContext('2d');
    
    // Définition de la largeur et de la hauteur du canevas
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    
    
    letters = letters.split('');
    
    // Configuration des colonnes
    let fontSize = 10,
        columns = canvas.width / fontSize;
    
    // Configuration des gouttes de pluie
    let drops = [];
    for (let i = 0; i < columns; i++) {
        drops[i] = 1;
    }
    
    let currentColorIndex = 0; // Index de la couleur actuelle
    
    // Fonction pour changer de couleur
    function changeColor() {
        currentColorIndex = (currentColorIndex + 1) % colors.length; // Incrémente l'index de couleur et revient à 0 lorsque toutes les couleurs ont été utilisées
    }
    
    // Fonction de dessin
    function draw() {
        ctx.fillStyle = 'rgba(0, 0, 0, .1)';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    
        for (let i = 0; i < drops.length; i++) {
            let text = letters[Math.floor(Math.random() * letters.length)];
            ctx.fillStyle = colors[(i + currentColorIndex) % colors.length]; // Sélectionne la couleur en alternance
            ctx.fillText(text, i * fontSize, drops[i] * fontSize);
            drops[i]++;
            if (drops[i] * fontSize > canvas.height && Math.random() > .95) {
                drops[i] = -10; // Place la goutte de pluie en dehors de l'écran pour la transition
                //changeColor(); // Appelle la fonction pour changer de couleur
            }
        }
    }
    
    // Boucle d'animation
    setInterval(draw, 33);
}

